set global foreign_key_checks=0;

drop table Losses;
drop table Writes;
drop table Authors;
drop table Borrows;
drop table Books;
drop table Members;

create table Members
(
	Id int primary key,
	First_Name varchar(20),
	Middle_Name varchar(20),
	Last_Name varchar(20),
	Address varchar(60),
	Expiry date,
	Dues int default 0
);

create table Books
(
	/*
	Shelf_Number(3)-Author_Genre(3)-Random(4)
	*/

	Id varchar(12) primary key,
	Name varchar(60),
	Shelf_Number varchar(3),
	Price int,
	Copies int
);

create table Borrows
(
	Member_Id int,
	Book_Id varchar(12),
	Borrow_Date date,
	Return_Date date,
	foreign key (Member_Id) references Members(Id) on update cascade on delete cascade,
	foreign key (Book_Id) references Books(Id) on update cascade on delete cascade
);

create table Authors
(
	/*
	Genre(3)-Random(3)
	*/
	Id varchar(7) primary key,
	Name varchar(60),
	Genre varchar(10)
);

create table Writes
(
	Book_Id varchar(12),
	Author_Id varchar(7),
	foreign key (Book_Id) references Books(Id) on update cascade on delete cascade,
	foreign key (Author_Id) references Authors(Id) on update cascade on delete cascade
);

create table Losses
(
	Book_Id varchar(12) references Books(Id),
	Member_Id int references Members(Id),
	foreign key (Book_Id) references Books(Id) on update cascade on delete cascade,
	foreign key (Member_Id) references Members(Id) on update cascade on delete cascade
)