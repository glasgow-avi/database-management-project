package sql;

import java.sql.*;

public class SQLInjector
{
    private static Connection conn;
    public static Statement statement;

    public static boolean connect(String password)
    {
        System.out.println("Trying to connect.");

        try
        {
            conn = DriverManager.getConnection("jdbc:mysql://localhost/library", "root", password);
            statement = conn.createStatement();
        } catch(SQLException e)
        {
            return false;
        }

        System.out.println("Connection successful");
        return true;
    }

    public static boolean executeStatement(String statement)
    {
        System.out.println("Trying to execute statement " + statement);
        try
        {
            SQLInjector.statement.execute(statement);
        } catch(SQLException e)
        {
            e.printStackTrace();
            System.out.println("Error");
            return false;
        }

        System.out.println("Statement executed");
        return true;
    }

    public static ResultSet executeSecondaryQuery(String query)
    {
        System.out.println("Trying to execute query " + query);
        ResultSet r = null;

        try
        {
            r = conn.createStatement().executeQuery(query);
        } catch(SQLException e)
        {
            e.printStackTrace();
            System.out.println("Error");
            return null;
        }

        System.out.println("Query success");
        return r;
    }

    public static ResultSet executeQuery(String query)
    {
        System.out.println("Trying to execute query " + query);
        ResultSet r = null;

        try
        {
            r = statement.executeQuery(query);
        } catch(SQLException e)
        {
            e.printStackTrace();
            System.out.println("Error");
            return null;
        }

        System.out.println("Query success");
        return r;
    }

    public static boolean isEmpty(ResultSet res_Set)
    {
        try
        {
            return !res_Set.next();
        } catch(SQLException e)
        {
            e.printStackTrace();
        }

        return false;
    }
}