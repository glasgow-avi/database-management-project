package sql;

import java.sql.SQLException;

public class InsertionError extends SQLException
{
    public String context;

    public InsertionError(String context)
    {
        this.context = context;
    }

    @Override
    public String toString()
    {
        return context;
    }
}
