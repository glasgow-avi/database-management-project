package gui.consoles.adders;

import gui.consoles.TableConsole;
import main.Main;
import sql.InsertionError;
import sql.SQLInjector;

import java.sql.ResultSet;

public class MemberAdder extends Editor$AdderConsole
{
    protected String editId;

    public MemberAdder(String editId)
    {
        super("Members");

        this.editId = editId;
    }

    @Override
    protected void addPairs()
    {
        getAttributes("Id", "Expiry", "Dues"); //build the list of attributes; id is automatically generated, Expiry_Date is systdate() + 20 years, Dues is 0.
        super.addPairs(); //set the layout, allocate memory, and change ui
    }

    @Override
    protected void insert() throws InsertionError
    {
        SQLInjector.executeStatement("start transaction");

        String firstName = textFields[0].getText(),
                middleName = textFields[1].getText(),
                lastName = textFields[2].getText(),
                address = textFields[3].getText();
        //the plebeian text fields

        if(editId != null)
        {
            if(!SQLInjector.executeStatement("update Members set First_Name = '" + firstName + "', Middle_Name = '" + middleName + "', Last_Name = '" + lastName + "', Address = '" + address + "' where Id = " + editId))
                throw new InsertionError("Check your input format.");

            SQLInjector.executeStatement("commit");

            dispose();
            Main.tableConsole.dispose();
            Main.tableConsole = new TableConsole("Members");
            return;
        }

        //id is generated automatically by the following convention
        String id = generateId();

        //check if that member already exists
        ResultSet r = SQLInjector.executeQuery("select * from Members where Id=" + id);
        if(!SQLInjector.isEmpty(r)) throw new InsertionError("This member already exists.");

        if(!SQLInjector.executeStatement("insert into Members values(" + id + ", '" + firstName + "', '" + middleName + "', '" + lastName + "', '" + address + "', Date_ADD(sysdate(), Interval 20 YEAR), 0)"))
            throw new InsertionError("Check your input format.");

        SQLInjector.executeStatement("commit");

        dispose();
        Main.tableConsole.dispose();
        Main.tableConsole = new TableConsole("Members");
    }
}