package gui.consoles.adders;

import gui.consoles.TableConsole;
import main.Main;
import sql.InsertionError;
import sql.SQLInjector;

import java.sql.ResultSet;

/***
 * This class is the console with the text fields and the buttons to add a book
 */
public class BorrowAdder extends Editor$AdderConsole
{
    public BorrowAdder()
    {
        super("Borrows");
    }


    @Override
    protected void addPairs()
    {
        getAttributes("Borrow_Date", "Return_Date"); //build the list of attributes; Borrow_Date is systdate() and Returned_Date is onUpadate().

        //set the layout, allocate memory, and change ui
        super.addPairs();
    }

    @Override
    protected void insert() throws InsertionError
    {
        SQLInjector.executeStatement("start transaction");

        String memberId = textFields[0].getText(), bookId = textFields[1].getText();
        //the plebeian text fields

        ResultSet resultSet; //a result set to perform checks on select statements before inserting

        //check if a book is not already borrowed by that member
        resultSet = SQLInjector.executeQuery("select * from Borrows where Member_Id = " + memberId + " and Return_Date is null");
        if(!SQLInjector.isEmpty(resultSet)) throw new InsertionError("This member has already borrowed a book.");

        //check if they're any existing copies of that book
        resultSet = SQLInjector.executeQuery("select * from Books where Id = '" + bookId + "' and Copies = 0");
        if(!SQLInjector.isEmpty(resultSet)) throw new InsertionError("There are no copies of that book remaining.");

        //check if the member's membership is expired
        resultSet = SQLInjector.executeQuery("select * from Members where Id = '" + memberId + "' and Expiry < sysdate()");
        if(!SQLInjector.isEmpty(resultSet)) throw new InsertionError("This member's membership has expired.");

        //the final insertion
        if(!SQLInjector.executeStatement("insert into Borrows(Member_Id, Book_Id, Borrow_Date) values(" + memberId + ", '" + bookId + "', sysdate())"))
            throw new InsertionError("Check your input format.");

//        try
//        {
//            if(SQLInjector.statement.getWarnings() != null) throw new InsertionError("No such Member / Book found  ");
//        } catch(SQLException e)
//        {
//            if(e instanceof InsertionError) throw (InsertionError)e;
//
//            e.printStackTrace();
//        }

        //reduce one copy of that book from the database
        if(!SQLInjector.executeStatement("update Books set Copies = Copies - 1 where Id = '" + bookId + "'")) throw new InsertionError("Something went wrong!");

        SQLInjector.executeStatement("commit");

        dispose();
        Main.tableConsole.dispose();
        Main.tableConsole = new TableConsole("Borrows");

    }
}