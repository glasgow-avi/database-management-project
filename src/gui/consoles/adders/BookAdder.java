package gui.consoles.adders;

import gui.consoles.TableConsole;
import main.Main;
import sql.InsertionError;
import sql.SQLInjector;

import java.sql.ResultSet;

/***
 * This class is the console with the text fields and the buttons to add a book
 */
public class BookAdder extends Editor$AdderConsole
{
    private String editId;

    public BookAdder(String editId)
    {
        super("Books");

        this.editId = editId;
    }

    @Override
    protected void addPairs()
    {
        getAttributes("Id"); //build the list of attributes; id is automatically generated

        //apart from the attributes of Books, these also have to be added
        attributes.add("Author_Name");
        attributes.add("Author_Genre");

        //set the layout, allocate memory, and change ui
        super.addPairs();
    }

    @Override
    protected void insert() throws InsertionError
    {
        SQLInjector.executeStatement("start transaction");

        String name = textFields[0].getText(),
                shelfNumber = textFields[1].getText(),
                price = textFields[2].getText(),
                copies = textFields[3].getText(),
                authorGenre = textFields[5].getText();
        //the plebeian text fields

        if(editId != null)
        {
            if(!SQLInjector.executeStatement("update Books set Name = '" + name + "', Shelf_Number = '" + shelfNumber + "', Price = " + price + ", Copies = " + copies + " where Id = '" + editId + "'"))
                throw new InsertionError("Check your input format.");

            SQLInjector.executeStatement("commit");

            dispose();
            Main.tableConsole.dispose();
            Main.tableConsole = new TableConsole("Books");
            return;
        }

        String[] authorNames = textFields[4].getText().split(", ");
        //multiple authors can be separated by ", "
        String[] authorIds = new String[authorNames.length];

        //generate the book id by this convention
        //TODO : Randomize the last block
        String bookId = firstThreeChars(shelfNumber) + "-" + firstThreeChars(authorGenre) + "-" + generateId();

        ResultSet rS; //a result set to perform checks on select statements before inserting

        //check if the book already exists
        rS = SQLInjector.executeQuery("select * from Books where Id = '" + bookId + "'");
        if(!SQLInjector.isEmpty(rS)) throw new InsertionError("Book already exists.");

        //the final insertion, return null if true, else return an Error.
        if(!SQLInjector.executeStatement("insert into Books values ('" + bookId + "', '" + name + "', '" + shelfNumber + "', " + price + ", " + copies + ")"))
            throw new InsertionError("Check your input format.");

        //iterate through the authors, do some checks
        for(int index = 0; index < authorNames.length; index++)
        {
            //calculate the author ids
            authorIds[index] = firstThreeChars(authorGenre) + "-" + generateId();
            //check if the author already exists
            rS = SQLInjector.executeQuery("select * from Authors where Id = '" + authorIds[index] + "'");

            //insert the author if the author doesn't exist already
            if(SQLInjector.isEmpty(rS))
                if(!SQLInjector.executeStatement("insert into Authors values ('" + authorIds[index] + "', '" + authorNames[index] + "', '" + authorGenre + "' )"))
                    throw new InsertionError("Check your input format.");

            //add the authors as writers
            SQLInjector.executeStatement("insert into Writes values ('" + bookId + "', '" + authorIds[index] + "')");
        }

        SQLInjector.executeStatement("commit");

        dispose();
        Main.tableConsole.dispose();
        Main.tableConsole = new TableConsole("Books");
    }
}