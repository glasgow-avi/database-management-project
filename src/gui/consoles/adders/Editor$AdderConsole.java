package gui.consoles.adders;

import com.mysql.jdbc.exceptions.MySQLSyntaxErrorException;
import gui.consoles.Console;
import gui.modified_components.Button;
import gui.modified_components.Label;
import gui.modified_components.Panel;
import sql.InsertionError;
import sql.SQLInjector;

import javax.swing.*;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/***
 * a generic class for all adder consoles
 */
public abstract class Editor$AdderConsole extends Console
{
    protected Label[] labels; //the textField labels
    protected JTextField[] textFields; //the actual fields in which the user enters the insert values

    private String tableName;

    private ResultSet resultSet; //a result set to hold the description of the table

    protected ArrayList<String> attributes = new ArrayList<>(); //the attributes of the main table

    protected Panel tablePanel = new Panel(); //the panel to hold the labels and text Fields

    protected Button doneButton = new Button("Done"); //the button that is clicked to insert into the table

    protected Queryer queryer;

    protected interface Queryer
    {
        void insert() throws InsertionError;
    }

    public Editor$AdderConsole(String tableName)
    {
        super("Add " + tableName); //set the title to Add tablename

        resultSet = SQLInjector.executeQuery("desc " + tableName); //get the description of the table

        this.tableName = tableName;
        setExtendedState(NORMAL); //make it a small window
        setSize(1, 1);
        setVisible(true);

        addPairs(); //add the textfield and label pairs

        pack();
        setVisible(true);

        doneButton.addActionListener(e -> {
            try
            {
                insert();
                JOptionPane.showMessageDialog(this, "Insertion successful.", "Success", 1);
            } catch(SQLException insertError)
            {
                //show an appropriate dialog pane
                SQLInjector.executeStatement("rollback");
                JOptionPane.showMessageDialog(this, insertError, "Error", 0);
            }
        });
    }

    /**
     * Method to add the textfield and label pairs
     */
    protected void addPairs()
    {
        //set the layout
        tablePanel.setLayout(new GridLayout(attributes.size(), 2, 20, 30));

        //allocate memory to the text Field and label arrays
        labels = new Label[attributes.size()];
        textFields = new JTextField[attributes.size()];

        for(int attributeIndex = 0; attributeIndex < attributes.size(); attributeIndex++)
        {
            labels[attributeIndex] = new Label(attributes.get(attributeIndex)); //initialize the Labels with an attribute name
            textFields[attributeIndex] = new JTextField(); //allocate memory to the text field
            tablePanel.add(labels[attributeIndex]);
            tablePanel.add(textFields[attributeIndex]);
        }

        add(tablePanel);
        add(doneButton);
    }

    /**
     * @param string the given string
     * @param strings the set of strings
     * @return Does a set of strings contain a given string?
     */
    private boolean contains(String string, String... strings)
    {
        for(String s : strings)
            if(string.equals(s))
                return true;

        return false;
    }

    /**
     * insert values into a table
     * @throw Was it successful or not
     */
    protected abstract void insert() throws InsertionError;

    protected void getAttributes(String... disallows)
    {
        try
        {
            while(resultSet.next())
            {
                if(!contains(resultSet.getString(1), disallows))
                    attributes.add(resultSet.getString(1)); //get the attribute name
            }
        } catch(SQLException e)
        {
            e.printStackTrace();
        }
    }

    protected String generateId()
    {
        resultSet = SQLInjector.executeQuery("select * from " + tableName + " order by " + ("Members".equals(tableName) ? "Id" : "substring(Id, 9, 3)") + " desc limit 1");

        try
        {
            resultSet.next();
            return String.valueOf(Integer.valueOf(tableName.equals("Members") ? resultSet.getString(1) : resultSet.getString(1).substring(8)) + 1);
        }
        catch(SQLException e)
        {

        }

        return "0";
    }

    /**
     * @param x A string
     * @return Gets the first three characters of a string and if a string is less than three characters long, returns the string
     */
    protected String firstThreeChars(String x)
    {
        switch(x.length())
        {
            case 0:
                return "000";
            case 1:
                return "00" + x;
            case 2:
                return "0" + x;
        }

        return x.substring(0, 3);
    }
}