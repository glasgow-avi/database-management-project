package gui.consoles;

import gui.UIConstants;

import javax.swing.*;
import java.awt.*;

/***
 * a generic Class for all Consoles
 */
public class Console extends JFrame
{
    public Console(String title)
    {
        setTitle(title); //set the title to that specified
        getContentPane().setBackground(UIConstants.backgroundColor); //set the background
        setVisible(true); //set it to visible
        setLayout(new FlowLayout()); //set the layout to flow layout
        setExtendedState(MAXIMIZED_BOTH); //set to full screen
    }

    //pack the Console with padding
    @Override
    public void pack()
    {
        super.pack();
        setSize(getWidth() + 50, getHeight() + 100);
    }
}
