package gui.consoles;

import gui.UIConstants;
import gui.consoles.adders.BookAdder;
import gui.consoles.adders.BorrowAdder;
import gui.consoles.adders.MemberAdder;
import gui.modified_components.Button;
import gui.modified_components.Label;
import gui.modified_components.Panel;
import main.Main;
import sql.SQLInjector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TableConsole extends Console
{
    private String tableName;
    private ArrayList<String> attributes = new ArrayList<>();

    private Label[][] table;

    private Panel tableContainer = new Panel();

    private JTextField searchBox = new JTextField();

    private Button addButton, deleteButton;

    public TableConsole(String tableName)
    {
        super("Library Manager");

        this.tableName = tableName;

        String query = "Books".equals(tableName) ? "select a.*, b.Name as Author_Name, b.Genre from Books a, Authors b, Writes c where a.Id = c.Book_Id and b.Id = c.Author_Id" : "select * from " + tableName;

        SQLInjector.executeStatement("drop view v");
        SQLInjector.executeStatement("create view v as " + query);

        ResultSet resultSet = SQLInjector.executeQuery("desc v");
        try
        {
            while(resultSet.next())
                attributes.add(resultSet.getString(1));
        } catch(SQLException e)
        {
            e.printStackTrace();
        }

        try
        {
//            if(!SQLInjector.isEmpty(resultSet))
                showAllData("select * from v", null);
        } catch(SQLException e)
        {
            e.printStackTrace();
        }

        Label searchLabel = new Label("Search");
        searchLabel.setBackground(UIConstants.backgroundColor);
        add(searchLabel);
        searchBox.setPreferredSize(new Dimension(200, 20));
        add(searchBox);

        if (Main.godMode)
        {
            addButton = new Button("Add");
            add(addButton);
            addButton.addActionListener(e -> {
                if (tableName.equals("Members"))
                    new MemberAdder(null);
                else if(tableName.equals("Books"))
                    new BookAdder(null);
                else if(tableName.equals("Borrows"))
                    new BorrowAdder();
            });
        }

        Panel fillPanel = new Panel();
        fillPanel.setPreferredSize(new Dimension(getWidth(), 20));
        add(fillPanel);

        add(tableContainer);

        searchBox.addActionListener(e -> {
            try
            {
                String q = "select * from v where ";
                String searchString = searchBox.getText();
                for(String attribute : attributes)
                    q += attribute + " like '%" + searchString + "%' or ";
                q = q.substring(0, q.length() - 3);
                showAllData(q, null);
            }
            catch(SQLException se)
            {
                se.printStackTrace();
            }
        });
    }

    protected void showAllData(String query, String sortingAttribute) throws SQLException
    {
        int numberOfTuples = 0;
        for(ResultSet allTuples = SQLInjector.executeQuery(query); allTuples.next(); numberOfTuples++); //count the number of queries

        if(numberOfTuples == 0)
            return;

        table = new Label[numberOfTuples][attributes.size()]; //initialze the table of labels
        //the number of rows is the number of tuples and the number of columns is the number of attributes

        tableContainer.removeAll(); //refresh the table panel
        tableContainer.setLayout(new GridLayout(table.length + 1, table[0].length, 20, 0)); //set the layout to grid
        //one extra row for a header
        //two extra columns for an edit and remove button

        for(String attribute : attributes) //loop to add the headers
        {
            Button header = new Button(attribute); //initialize a label with an attribute name
            header.setOpaque(true);
            tableContainer.add(header);
            header.addActionListener(new Sorter(this, query));
        }

        if(Main.godMode)
        {
            tableContainer.add(new Panel());
            tableContainer.add(new Panel());
        }

        int rowIndex = 0;
        for(ResultSet allTuples = SQLInjector.executeQuery(query + (sortingAttribute == null ? "" : " order by " + sortingAttribute)); allTuples.next(); rowIndex++)
        {
            for(int attributeIndex = 1; attributeIndex <= attributes.size(); attributeIndex++)
                table[rowIndex][attributeIndex - 1] = new Label(allTuples.getString(attributeIndex));

            if(tableName.equals("Members"))
            {
                ResultSet overdues = SQLInjector.executeSecondaryQuery("select sum(overdue) from (select *, case when Return_Date is NULL then DateDiff(sysdate(), Date_Add(Borrow_Date, INTERVAL 60 Day)) else DateDiff(Return_Date, Date_Add(Borrow_Date, INTERVAL 60 Day)) end as overdue from Borrows)Overdues where overdue > 0 and Overdues.Member_Id = " + table[rowIndex][0].getText());
                overdues.next();
                String dues = overdues.getString(1);
                table[rowIndex][attributes.size() - 1] = new Label(String.valueOf(Integer.valueOf(table[rowIndex][attributes.size() - 1].getText()) + (dues == null ? 0 : Integer.valueOf(dues))));
            }
        }

        for(Label[] tuple : table)
        {
            for(Label placeholder : tuple)
            {
                tableContainer.add(placeholder);
            }

            if(Main.godMode)
            {
                if(!"Borrows".equals(tableName) && !"Losses".equals(tableName))
                {
                    Button editButton = new Button("Edit");
                    editButton.addActionListener(e ->
                    {
                        if (tableName.equals("Members"))
                            new MemberAdder(tuple[0].getText());
                        else if(tableName.equals("Books"))
                            new BookAdder(tuple[0].getText());
                    });
                    tableContainer.add(editButton);
                    deleteButton = new Button("Remove");
                    deleteButton.addActionListener(e -> {
                        if(JOptionPane.showConfirmDialog(this, "Are you sure you want to delete entry " + tuple[0].getText() + "?", "Please confirm", JOptionPane.YES_NO_OPTION) != 0)
                            return;
                        boolean success = SQLInjector.executeStatement("delete from " + tableName + " where Id = '" + tuple[0].getText() + "'");
                        if(!success) SQLInjector.executeStatement("delete from " + tableName + " where Id = " + tuple[0].getText());

                        this.dispose();
                        new TableConsole(tableName);
                    });
                    tableContainer.add(deleteButton);
                }

                else if("Borrows".equals(tableName))
                {
                    deleteButton = new Button("Mark as returned");
                    Button lostButton = new Button("Mark as lost");
                    deleteButton.addActionListener(e -> {
                        SQLInjector.executeStatement("update Books set Copies = Copies + 1 where Id = '" + tuple[1].getText() + "'");
                        SQLInjector.executeStatement("update " + tableName + " set Return_Date=sysdate() where Member_Id = " + tuple[0].getText() + " and Return_Date is NULL");
                        this.dispose();
                        new TableConsole(tableName);
                    });
                    lostButton.addActionListener(e -> {
                        SQLInjector.executeStatement("update " + tableName + " set Return_Date=sysdate() where Member_Id = " + tuple[0].getText() + " and Return_Date is NULL");
                        SQLInjector.executeStatement("insert into Losses values ('" + tuple[1].getText() + "', " + tuple[0].getText() + ")");
                        SQLInjector.executeStatement("update Members set Dues = Dues + (select Price from Books where Id = '" + tuple[1].getText() + "') where Id = " + tuple[0].getText());
                        this.dispose();
                        new TableConsole(tableName);
                    });
                    tableContainer.add(lostButton);
                    if(!"".equals(tuple[3].getText()))
                    {
                        deleteButton.setEnabled(false);
                        lostButton.setEnabled(false);
                    }
                    tableContainer.add(deleteButton);
                }
                else
                {
                    deleteButton = new Button("Mark as returned");
                    deleteButton.addActionListener(e -> {
                        SQLInjector.executeStatement("update Books set Copies = Copies + 1 where Id = '" + tuple[0].getText() + "'");
                        SQLInjector.executeStatement("update Members set Dues = Dues - (select Price from Books where Id = '" + tuple[0].getText() + "') where Id = " + tuple[1].getText());
                        SQLInjector.executeStatement("delete from Losses where Book_Id = '" + tuple[0].getText() + "' and Member_Id = " + tuple[1].getText());
                        this.dispose();
                        new TableConsole(tableName);
                    });
                    tableContainer.add(deleteButton);
                    tableContainer.add(new Panel());
                }
            }
        }

        repaint();
        setVisible(true);
    }
}

class Sorter implements ActionListener
{
    private TableConsole openWindow;
    private String query;

    public Sorter(TableConsole openWindow, String query)
    {
        this.openWindow = openWindow;
        this.query = query;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try
        {
            openWindow.showAllData(query, ((Button)e.getSource()).getText());
            openWindow.setPreferredSize(openWindow.getPreferredSize());

        } catch(SQLException e1)
        {
            e1.printStackTrace();
        }
    }
}