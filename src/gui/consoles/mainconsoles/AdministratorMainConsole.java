package gui.consoles.mainconsoles;

import gui.UIConstants;
import gui.consoles.Console;
import gui.consoles.TableConsole;
import gui.modified_components.Button;
import gui.modified_components.Label;
import main.Main;

import javax.swing.*;
import java.awt.GridLayout;

public class AdministratorMainConsole extends Console
{
    //The tables which can be edited
    private static final String[] tables = {"Members", "Books", "Borrows", "Losses"};

    //Constructor
    public AdministratorMainConsole()
    {
        super("Library Manager"); //set the title
        setDefaultCloseOperation(EXIT_ON_CLOSE); //the app exits on closing this

        setLayout(new GridLayout(tables.length + 1, 1, 40, 80)); //1 extra row for a header

        Label headerLabel = new Label("Manage Table", true); //The heading
        headerLabel.setHorizontalAlignment(SwingConstants.CENTER); //centralize the heading
        headerLabel.setFont(UIConstants.headerFont); //set its font
        headerLabel.setBackground(UIConstants.backgroundColor); //set its background
        add(headerLabel);

        for(String table : tables) //loop to add buttons to go to a table
        {
            Button selectTableButton = new Button(table); //initialize the button with the table name
            selectTableButton.addActionListener(e -> {
                try
                {
                    Main.tableConsole.dispose();
                }
                catch (NullPointerException viewsNeverCreated)
                {

                }
                Main.tableConsole = new TableConsole(table);
            }); //make a console with that table information on click
            add(selectTableButton); //add the button to the panel
            selectTableButton.setBackground(UIConstants.buttonColour); //change the background colour of the button
            selectTableButton.setFont(UIConstants.subHeaderFont); //change the font of the button
        }
    }
}
