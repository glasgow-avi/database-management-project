package gui.consoles.mainconsoles;

import gui.consoles.TableConsole;

/***
 * for the User to view the books.
 */
public class UserMainConsole extends TableConsole
{
    public UserMainConsole()
    {
        super("Books");

        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
