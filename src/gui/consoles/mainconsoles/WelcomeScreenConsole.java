package gui.consoles.mainconsoles;

import gui.UIConstants;
import gui.consoles.Console;
import gui.modified_components.Button;
import gui.modified_components.Label;
import gui.modified_components.Panel;
import main.Main;
import sql.SQLInjector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/***
 * The welcome screen
 */
public class WelcomeScreenConsole extends Console
{
    private Panel[] panels = new Panel[5]; //each component is on a panel because of size convenience issues

    private Label welcomeText, enterPasswordLabel, orLabel; //the different text displayed on the window
    private Button continueAsGuestButton; //a button to continue as a guest
    protected static JPasswordField passwordTextField; //a password field to enter your password

    public WelcomeScreenConsole()
    {
        super("Library Manager");

        setDefaultCloseOperation(EXIT_ON_CLOSE); //on closing this window the app exits

        setLayout(new GridLayout(5, 1));

        for(int index = 0; index < panels.length; index++)
        {
            panels[index] = new Panel();
            add(panels[index]);
        }

        //UI Stuff
        welcomeText = new Label("Welcome to MIT Library", true);
        welcomeText.setFont(UIConstants.headerFont);
        welcomeText.setBackground(UIConstants.backgroundColor);
        panels[0].add(welcomeText);

        enterPasswordLabel = new Label("Enter password to login", true);
        enterPasswordLabel.setFont(UIConstants.subHeaderFont);
        enterPasswordLabel.setBackground(UIConstants.backgroundColor);
        panels[1].add(enterPasswordLabel);

        passwordTextField = new JPasswordField();
        passwordTextField.setPreferredSize(new Dimension(500, 40));
        panels[2].add(passwordTextField);
        passwordTextField.addActionListener(new LoginListener(true));

        orLabel = new Label("OR");
        orLabel.setFont(UIConstants.subHeaderFont);
        orLabel.setBackground(UIConstants.backgroundColor);
        panels[3].add(orLabel);

        continueAsGuestButton = new Button("Login as guest");
        continueAsGuestButton.setBackground(UIConstants.labelColour);
        continueAsGuestButton.addActionListener(new LoginListener(false));
        continueAsGuestButton.setFont(UIConstants.subHeaderFont);
        continueAsGuestButton.setPreferredSize(new Dimension(350, 40));
        panels[4].add(continueAsGuestButton);

        repaint();
        setVisible(true);
    }
}

//Action listener to login on pressing the enter key or the login as guest button
class LoginListener implements ActionListener
{
    private boolean administrator; //Is the user trying to log in as an administrator

    public LoginListener(boolean administrator)
    {
        this.administrator = administrator;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        boolean connectionValidity;

        if(administrator)
        {
            connectionValidity = SQLInjector.connect(String.valueOf(WelcomeScreenConsole.passwordTextField.getPassword()));
            //try and connect using the password

            if(!connectionValidity) //try again, wrong password
                JOptionPane.showMessageDialog(Main.welcomeScreenConsole, "Incorrect Password", "Error", 0);
            else
            //go to the main app
            {
                Main.mainWindow = new AdministratorMainConsole();
                Main.welcomeScreenConsole.dispose();
                Main.godMode = true;
            }
        }

        else
        {
            //login as administrator, and the data abstraction is handled by the front-end
            connectionValidity = SQLInjector.connect("Pinkfloyd1**");

            if(!connectionValidity)
                //something that shouldn't be happening, happened.
                JOptionPane.showMessageDialog(Main.welcomeScreenConsole, "Check your connection", "Error", 0);
            else
            //go to the main app.
            {
                Main.mainWindow = new UserMainConsole();
                Main.welcomeScreenConsole.dispose();
            }
        }
    }
}