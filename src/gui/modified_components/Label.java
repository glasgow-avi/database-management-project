package gui.modified_components;

import gui.UIConstants;

import javax.swing.*;
import java.util.regex.Pattern;

public class Label extends JLabel
{
    private String data;
    private boolean tupleLabel;

    private void initialize()
    {
        setOpaque(true);
        setBackground(UIConstants.labelColour);
        setForeground(UIConstants.smallTextColor);
        try
        {
            setHorizontalAlignment(Pattern.matches("[0-9]+", getText()) ? SwingConstants.RIGHT : SwingConstants.LEFT);
        }
        catch (NullPointerException dontKnowWhatHappensHere)
        {

        }

        if(tupleLabel)
            setToolTipText(data);
    }

    public Label()
    {
        initialize();
    }

    public Label(String title, boolean bigLabel)
    {
        super();
        this.tupleLabel = !bigLabel;
        setText(title);
        initialize();
    }

    public Label(String title)
    {
        super();
        this.tupleLabel = true;
        setText(title);
        initialize();
    }

    @Override
    public void setText(String text)
    {
        data = text;

        if(!tupleLabel)
        {
            super.setText(text);
            return;
        }

        try
        {
            super.setText(text.substring(0, 10));
        }
        catch(NullPointerException t)
        {
            
        }
        catch(StringIndexOutOfBoundsException s)
        {
            super.setText(text);
        }
    }
}