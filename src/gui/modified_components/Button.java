package gui.modified_components;

import gui.UIConstants;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Button extends JButton
{
    private void initialize()
    {
        setBackground(UIConstants.buttonColour);
        setForeground(UIConstants.smallTextColor);

        setBorderPainted(true);
        setContentAreaFilled(false);
    }

    public Button()
    {
        initialize();
    }

    public Button(String title)
    {
        super(title);
        initialize();
    }

    protected void paintComponent(Graphics g)
    {
        if (getModel().isArmed())
            g.setColor(Color.lightGray);
        else
            g.setColor(getBackground());

        g.fillRoundRect(0, 0, getSize().width - 1, getSize().height - 1, 25, 25);

        super.paintComponent(g);
    }

    protected void paintBorder(Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(getForeground());
        g2.setStroke(new BasicStroke(4));
        g2.drawRoundRect(0, 0, getSize().width - 1, getSize().height - 1, 25, 25);
    }

    private Shape shape;

    public boolean contains(int x, int y)
    {
        if (shape == null || !shape.getBounds().equals(getBounds()))
            shape = new Ellipse2D.Float(0, 0, getWidth(), getHeight());

        return shape.contains(x, y);
    }
}