package gui.modified_components;

import gui.UIConstants;

import javax.swing.*;

public class Panel extends JPanel
{
    public Panel()
    {
        super();

        setOpaque(true);
        setBackground(UIConstants.backgroundColor);
    }
}
