package gui;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public class UIConstants
{
    private static final String font = "Garuda";
    public static final Color backgroundColor = new Color(0xffffffff), buttonColour = new Color(0xffa000), labelColour = new Color(0xffa011),
    smallTextColor = new Color(0x333333);

    public static Font headerFont, subHeaderFont, smallFont;

    static
    {
        try
        {
            Font font = Font.createFont(Font.TRUETYPE_FONT, new File("res/fonts/" + UIConstants.font + ".otf"));
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);
        } catch(FontFormatException e)
        {
            e.printStackTrace();
        } catch(IOException e)
        {

        }

        headerFont = new Font(font, Font.BOLD, 72);
        subHeaderFont = new Font(font, 0, 24);
    }
}
